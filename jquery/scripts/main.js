var mainContent     = $('#content');
var homeButton      = $('#home');
var usersButton     = $('#users');
var toDosButton     = $('#todos');
var homeContent;

$( document ).ready(function() {
    homeContent = mainContent.html();
});

function cleanButtons() {
    $('.btn').each(function() {
        $( this ).removeClass( "focus" );
    });    
}

function oldWayGetUsersData(e) {
    e.preventDefault();

    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/users',
        success: function(response) {
            
            //Adding focus
            cleanButtons();
            $(this).addClass('focus');
                
            var liTemplate = '';
            response.forEach(user => {
                liTemplate += `<li class="list-group-item">${user.name} - ${user.email}</li>`;
            });

            var usersTemplate = `
                <h3>Users List</h3>
                <ul class="list-group">
                    ${liTemplate}
                    <button>Items</button>
                </ul>
            `;
                
            mainContent.html(usersTemplate)

        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    })
    usersItems()
}

function usersItems (){
    //otro ajax
    console.log('traigame los botones');
}

async function getToDos (args) {
    const result = await $.ajax({
        url: 'https://jsonplaceholder.typicode.com/todos'
    });

    return result;
}

homeButton.on('click', function (e) {
    cleanButtons();
    $(this).addClass('focus');

    mainContent.html(homeContent) 
});

usersButton.on('click', oldWayGetUsersData);
toDosButton.on('click', addToDos);

function addToDos () {
    
    //Adding focus
    cleanButtons();
    $(this).addClass('focus');

    getToDos().then(data => {
    

        let liTemplate = '';
        data.forEach(todo => {
            liTemplate += `
                <li class="list-group-item">
                    ${todo.title}</br>
                    <span>Completed = </span>
                    <span class="badge">${todo.completed}</span> 
                </li>`;
        });

        let usersTemplate = `
            <h3>To Do List</h3>
            <ul class="list-group">
                ${liTemplate}
            </ul>
        `;
        
        mainContent.html(usersTemplate) 
    })
}


