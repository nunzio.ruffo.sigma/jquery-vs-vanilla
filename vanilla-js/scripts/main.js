var mainContent     = document.getElementById('content');
var homeButton      = document.getElementById('home');
var usersButton     = document.getElementById('users');
var toDosButton     = document.getElementById('todos');
var homeContent;


window.onload = function() {
    homeContent = document.getElementById('content').innerHTML;
};

const cleanButtons = function () {
    let buttons = document.getElementsByClassName('btn');

    for (i = 0; i < buttons.length; i++){
        buttons[i].classList.remove('focus');
    }
}

const getUsersData = async function () {
    
    const url = 'https://jsonplaceholder.typicode.com/users'    
    let response = await fetch(url);
    let result = await response.json();
    return result;
}

const getToDos = async function () {
    
    const url = 'https://jsonplaceholder.typicode.com/todos'    
    let response = await fetch(url);
    let result = await response.json();
    return result
}

const notifyMe = function () {
    alert('Notice me!')
}

// Click Events
document.getElementById("notify").addEventListener("click", function () {
    notifyMe();
})

homeButton.addEventListener("click", function (e) {
    
    //Adding focus
    cleanButtons();
    this.classList.add('focus');

    e.preventDefault(); 
    mainContent.innerHTML = homeContent;
});

usersButton.addEventListener("click", function (e) {
    e.preventDefault();
    
    //Adding focus
    cleanButtons();
    this.classList.add('focus');
    
    let usersData = getUsersData();
    usersData.then(users => {
        
        let liTemplate = '';
        users.forEach(user => {
            liTemplate += `<li class="list-group-item">${user.name} - ${user.email}</li>`;
        });

        let template = `
            <h3>Users List</h3>
            <ul class="list-group">
                ${liTemplate}
            </ul>
        `;
        
        mainContent.innerHTML = template;
            
    }).catch(error => {
        console.log(error);
    })
});

toDosButton.addEventListener("click", function (e) {
    e.preventDefault();

    //Adding focus
    cleanButtons();
    this.classList.add('focus');
    
    let toDosData = getToDos();
    toDosData.then(data => {
        
        let liTemplate = '';
        data.forEach(todo => {
            liTemplate += `
                <li class="list-group-item">
                    ${todo.title}</br>
                    <span>Completed = </span>
                    <span class="badge">${todo.completed}</span> 
                </li>`;
        });

        let template = `
            <h3>To Do List</h3>
            <ul class="list-group">
                ${liTemplate}
            </ul>
        `;
        
        mainContent.innerHTML = template;
            
    }).catch(error => {
        console.log(error);
    })
});